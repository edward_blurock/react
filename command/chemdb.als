ROOT                   /Users/edwardblurock/Reaction/REACT
DATA                   /Users/edwardblurock/Reaction/REACT/data
DBDIRECTORY            /Users/edwardblurock/Reaction/REACT/data/DB
MOLDIR                 /Users/edwardblurock/Reaction/REACT/data/mol
RXNDIR                 /Users/edwardblurock/Reaction/REACT/data/rxn
PERFDIR                /Users/edwardblurock/Reaction/REACT/data/mech
TABLEDIR               /Users/edwardblurock/Reaction/REACT/data/tables

MOLECULEROOT           mol/molecule
REACTIONROOT           rxn/reaction
SUBSTRUCTUREROOT       mol/substructure

BENSONROOT             BensonStandard
BENSONTREEROOT         bensontree
CURRENT                .

DBDataDirectory               (DbaseOps)DBDataDirectory
DBDataMolRoot                 (DbaseOps.Molecules)DBDataMolRoot
DBDataSubstructureRoot        (DbaseOps.Substructures)DBDataSubstructureRoot
DBDirectory                   (CreateOpenClose)DBDirectory
TablesDataDirectory           (Tables)TablesDataDirectory
TablesOutputDirectory         (Tables)TablesOutputDirectory
RootMolName                   (Mol)RootMolName
MolDirectory                  (Mol)MolDirectory
MolOutName                    (Mol)MolOutName
MolOutDir                     (Mol)MolOutDir

exit(0)
