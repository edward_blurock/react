#ifndef SELLO_BOND_H
#define SELLO_BOND_H

#include "bondmat.h"

extern BondOrderMatrix *MFMol2BndOrdMat( MolFileMolecule *molecule );
extern ConjAtomInfoVec *MFMol2CnjAtmInf( MolFileMolecule *molecule );




#endif
